﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SimpleWifi;
using SimpleWifi.Win32;
using System.Net;
using System.Threading;
using System.Configuration;

namespace cameracontroller
{
    public class Program
    {
        private static Wifi wifi;

        static void Main(string[] args)
        {
            // Init wifi object and event handlers
            wifi = new Wifi();
            
            if (wifi.NoWifiAvailable)
                Console.WriteLine("\r\n-- NO WIFI CARD WAS FOUND --");

            var cameraNamePrefix = ConfigurationManager.AppSettings["CAMERA_NAME_PREFIX"];

            Console.WriteLine("searching for camera's with wifi name prefix == {0}", cameraNamePrefix);

            // loop through each access point that starts with the camera name prefix specified in configuraiton file, example: "3dpico"
            IEnumerable<AccessPoint> accessPoints = wifi.GetAccessPoints().Where(ap => ap.Name.StartsWith(cameraNamePrefix)).OrderByDescending(ap => ap.SignalStrength);

            Console.WriteLine("found {0} cameras that match", accessPoints.Count());

            foreach (AccessPoint ap in accessPoints)
            {
                try
                {
                    // Connect to the camera
                    Connect(ap);
                    // wait 3 seconds
                    Thread.Sleep(3000);
                    // print connection status
                    Status();
                    DownloadFiles(ap.Name);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("failed to download files from {0}: reason : {1}", ap.Name, ex.Message);
                }
            }
                
            
        }

        static void Connect(AccessPoint selectedAP)
        {
            // Auth
            AuthRequest authRequest = new AuthRequest(selectedAP);

            if (authRequest.IsPasswordRequired)
            {
                var cameraNamePrefix = ConfigurationManager.AppSettings["CAMERA_NAME_PREFIX"];
                var passwordPrefix = ConfigurationManager.AppSettings["PASSWORD_PREFIX"];
                authRequest.Password = selectedAP.Name.Replace(cameraNamePrefix, passwordPrefix);   
            }

            selectedAP.ConnectAsync(authRequest, true, OnConnectedComplete);

            Console.WriteLine("\r\nConnecting to camera {0}", selectedAP.Name);
        }

        private static void Status()
        {
            Console.WriteLine("\r\n-- CONNECTION STATUS --");
            if (wifi.ConnectionStatus == WifiStatus.Connected)
                Console.WriteLine("connected!");
            else
                Console.WriteLine("You are not connected to a wifi");
        }

        private static void DownloadFiles(string cameraid) {
            using (WebClient client = new WebClient())
            {
                var rootPath = ConfigurationManager.AppSettings["ROOT_VIDEO_DOWNLOAD_PATH"];
                var url = ConfigurationManager.AppSettings["GOPRO_CAMERA_VIDEOS_URL"];
                var html = client.DownloadString(url);
                IList<string> filenames = ExtractVideoFileNames(html);
                var baseUrl = url + "{0}.mp4";
                foreach(var f in filenames){
                    var downloadUrl = string.Format(baseUrl, f);
                
                    var dir = string.Format("c:\\temp\\{0}\\", cameraid);
                    if (!System.IO.Directory.Exists(dir))
                        System.IO.Directory.CreateDirectory(dir);

                    var filePath = string.Format("{0}{1}.mp4",dir,f);
                    //if (!System.IO.File.Exists(filePath))
                    //{
                        DateTime start = DateTime.Now;
                        Console.WriteLine("Downloading {0} => {1}", downloadUrl, filePath);
                        client.DownloadFile(new Uri(downloadUrl), filePath);
                        TimeSpan totalDownloadtime = DateTime.Now.Subtract(start);
                        Console.WriteLine("Completed download : {0} seconds",totalDownloadtime.Seconds);
                    //}
                    //else {
                    //    Console.WriteLine("file {0} already exists, skipping download", filePath);
                    //}
                } 
               
            }
        }

        private static IList<string> ExtractVideoFileNames(string html)
        {
            IList<string> files = new List<string>();
            
            Regex r = new Regex("href=\"(?<filename>.*).MP4\"");

            // Match the regular expression pattern against a text string.
            Match m = r.Match(html);

            while (m.Success) 
            {
                string filename = m.Groups["filename"].Value;

                if (!string.IsNullOrEmpty(filename))
                {
                    files.Add(filename);
                }

                m = m.NextMatch();
            }

            return files;
        }



        static void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
        
        }

        static void OnConnectedComplete(bool success)
        {
          
        }

    }
}
